#!/bin/sh

for jsonFile in ./data/*/*.json; do
  echo "importing ${jsonFile}";
  col=`echo "${jsonFile//*\/}" | cut -d '.' -f 1`;
  db=`echo ${jsonFile} | cut -d/ -f 3`;
  mongoimport --host mongo --port 27017 --db $db --collection $col --file $jsonFile --type json --jsonArray;
done