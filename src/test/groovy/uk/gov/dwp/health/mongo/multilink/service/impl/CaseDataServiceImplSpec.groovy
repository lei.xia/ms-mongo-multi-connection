package uk.gov.dwp.health.mongo.multilink.service.impl

import spock.lang.Specification
import uk.gov.dwp.health.mongo.multilink.db.fhacase.Case
import uk.gov.dwp.health.mongo.multilink.repositories.fhacase.CaseRepository

class CaseDataServiceImplSpec extends Specification {

    private CaseDataServiceImpl underTest
    private CaseRepository caseRepositoryStub

    void 'when listAll called, case repository findAll invoked only once'() {
        setup: 'setup test subject'
        caseRepositoryStub = Mock()
        underTest = new CaseDataServiceImpl(caseRepositoryStub)

        when: 'test subject listAll called'
        underTest.listAll()

        then: 'repository findAll should called only once'
        1 * caseRepositoryStub.findAll()

        cleanup: 'clean up'
        caseRepositoryStub = null
        underTest = null
    }

    void 'when listAll called, case repository findAll invoked and return 2 cases'() {
        setup: 'setup test subject'
        caseRepositoryStub = Stub {
            findAll() >> [new Case()]
        }
        underTest = new CaseDataServiceImpl(caseRepositoryStub)

        when: 'test subject listAll called'
        List<Case> actual = underTest.listAll()

        then: 'repository findAll should called only once'
        actual.size() == 1

        and: 'verify item of return types'
        actual.forEach { it ->
            assert it instanceof Case
        }

        cleanup: 'clean up'
        caseRepositoryStub = null
        underTest = null
    }
}
