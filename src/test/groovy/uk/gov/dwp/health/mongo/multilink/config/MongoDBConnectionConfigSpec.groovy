package uk.gov.dwp.health.mongo.multilink.config

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest
class MongoDBConnectionConfigSpec extends Specification {

    @Autowired
    private MongoDBConnectionConfig underTest

    void 'should case repository factory has all mongo details'() {
        expect: 'database name to be fhacase'
        underTest.caseRepoFactory().db.name == 'fhacase'
        and: 'template is pointing to correct db'
        underTest.caseMongoTemplate().db.name == 'fhacase'
    }

    void 'should appointment repository factory has all mongo details'() {
        expect: 'database name to be fhaappointment'
        underTest.appointmentRepoFactory().db.name == 'fhaappointment'
        and: 'template is pointing to correct db'
        underTest.appointmentMongoTemplate().db.name == 'fhaappointment'
    }
}
