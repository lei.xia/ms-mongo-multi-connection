package uk.gov.dwp.health.mongo.multilink

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.ApplicationContext
import spock.lang.Specification

@SpringBootTest
class MultiLinkApplicationTest extends Specification {

    @Autowired
    private ApplicationContext underTest

    def 'should application context load'() {
        expect: 'application is not null'
        underTest
    }
}

