package uk.gov.dwp.health.mongo.multilink.service.impl

import spock.lang.Specification
import spock.lang.Stepwise
import uk.gov.dwp.health.mongo.multilink.db.fhaappointment.Appointment
import uk.gov.dwp.health.mongo.multilink.db.fhacase.Case
import uk.gov.dwp.health.mongo.multilink.repositories.fhaappointment.AppointmentRepository

@Stepwise
class AppointmentDataServiceImplSpec extends Specification {

    private AppointmentDataServiceImpl underTest
    private AppointmentRepository appointmentRepositoryStub

    void 'when listAll called, the repository findAll invoked only once'() {
        setup: 'test subject and mock'
        appointmentRepositoryStub = Mock()
        underTest = new AppointmentDataServiceImpl(appointmentRepositoryStub)

        when: 'test subject listAll called'
        underTest.listAll()

        then: 'verify repository findAll called only once'
        1 * appointmentRepositoryStub.findAll()

        cleanup: 'clean up'
        appointmentRepositoryStub = null;
        underTest = null
    }

    void 'when listAll called, the repository findAll invoked only once and 2 records returned'() {
        setup: 'test subject and stub'
        appointmentRepositoryStub = Stub {
            findAll() >> [new Appointment(), new Appointment()]
        }
        underTest = new AppointmentDataServiceImpl(appointmentRepositoryStub)

        when: 'test subject listAll called'
        List<Case> actual = underTest.listAll()

        then: 'verify result'
        actual.size() == 2

        and: 'verify item of return object'
        actual.forEach { it ->
            assert it instanceof Appointment
        }
        cleanup: 'clean up'
        appointmentRepositoryStub = null;
        underTest = null
    }
}
