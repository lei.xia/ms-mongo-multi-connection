package uk.gov.dwp.health.mongo.multilink.config;

import com.mongodb.MongoClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;

@Configuration
public class MongoDBConnectionConfig {

    private String host;
    private Integer port;
    private String caseDb;
    private String appointmentDb;

    public MongoDBConnectionConfig(@Value("${mongo.host}") String host,
                                   @Value("${mongo.port}") Integer port,
                                   @Value("${mongo.dbs.case}") String caseDb,
                                   @Value("${mongo.dbs.appointment}") String appointmentDb) {
        this.host = host;
        this.port = port;
        this.caseDb = caseDb;
        this.appointmentDb = appointmentDb;
    }

 
    @Bean(name = "caseMongoTemplate")
    public MongoTemplate caseMongoTemplate() throws Exception {
        return new MongoTemplate(caseRepoFactory());
    }

    @Bean(name = "appointmentMongoTemplate")
    public MongoTemplate appointmentMongoTemplate() throws Exception {
        return new MongoTemplate(appointmentRepoFactory());
    }

    @Bean
    public MongoDbFactory caseRepoFactory() throws Exception {
        return new SimpleMongoDbFactory(new MongoClient(host, port),
                caseDb);
    }

    @Bean
    public MongoDbFactory appointmentRepoFactory() throws Exception {
        return new SimpleMongoDbFactory(new MongoClient(host, port),
                appointmentDb);
    }
}
