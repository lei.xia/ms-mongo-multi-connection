package uk.gov.dwp.health.mongo.multilink.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uk.gov.dwp.health.mongo.multilink.db.fhaappointment.Appointment;
import uk.gov.dwp.health.mongo.multilink.repositories.fhaappointment.AppointmentRepository;
import uk.gov.dwp.health.mongo.multilink.service.DataService;

import java.util.List;

@Service(value = "appointmentDataServiceImpl")
public class AppointmentDataServiceImpl implements DataService<Appointment> {

    private AppointmentRepository repository;

    public AppointmentDataServiceImpl(@Autowired AppointmentRepository repository) {
        this.repository = repository;
    }

    public List<Appointment> listAll() {
        return repository.findAll();
    }
}
