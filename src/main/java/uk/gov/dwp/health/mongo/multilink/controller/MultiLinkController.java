package uk.gov.dwp.health.mongo.multilink.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import uk.gov.dwp.health.mongo.multilink.db.fhaappointment.Appointment;
import uk.gov.dwp.health.mongo.multilink.db.fhacase.Case;
import uk.gov.dwp.health.mongo.multilink.service.DataService;

import java.util.List;

@Controller
public class MultiLinkController {

    private DataService caseDataService;
    private DataService appointmentDataService;

    @Autowired
    public MultiLinkController(@Qualifier(value = "caseDataServiceImpl") DataService caseDataService,
                               @Qualifier(value = "appointmentDataServiceImpl") DataService appointmentDataService
    ) {
        this.caseDataService = caseDataService;
        this.appointmentDataService = appointmentDataService;
    }

    @RequestMapping(value = "/status", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> status() {
        return ResponseEntity.ok().body("OK");
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/cases", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<Case>> listAllCase() {
        return ResponseEntity.ok().body(caseDataService.listAll());
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/appointments", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<Appointment>> listAllAppointments() {
        return ResponseEntity.ok().body(appointmentDataService.listAll());
    }
}
