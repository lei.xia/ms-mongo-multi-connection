package uk.gov.dwp.health.mongo.multilink.db.fhacase;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "case")
public class Case {

    @Id
    private String id;
    private String benefit_type;
    private String supplier;
    private Integer ref_id;
}
