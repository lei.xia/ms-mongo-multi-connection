package uk.gov.dwp.health.mongo.multilink.repositories.fhaappointment;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import uk.gov.dwp.health.mongo.multilink.db.fhaappointment.Appointment;

@Repository(value = "appointmentRepository")
public interface AppointmentRepository extends MongoRepository<Appointment, String> {
}
