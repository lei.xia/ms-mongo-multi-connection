package uk.gov.dwp.health.mongo.multilink.repositories.fhacase;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import uk.gov.dwp.health.mongo.multilink.db.fhacase.Case;

@Repository(value = "caseRepository")
public interface CaseRepository extends MongoRepository<Case, String> {
}
