package uk.gov.dwp.health.mongo.multilink.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
@EnableMongoRepositories(basePackages = "uk.gov.dwp.health.mongo.multilink.repositories.fhaappointment",
        mongoTemplateRef = "appointmentMongoTemplate")
public class AppointmentMongoDBConfig {
}
