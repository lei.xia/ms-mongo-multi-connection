package uk.gov.dwp.health.mongo.multilink.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uk.gov.dwp.health.mongo.multilink.db.fhacase.Case;
import uk.gov.dwp.health.mongo.multilink.repositories.fhacase.CaseRepository;
import uk.gov.dwp.health.mongo.multilink.service.DataService;

import java.util.List;

@Service(value = "caseDataServiceImpl")
public class CaseDataServiceImpl implements DataService<Case> {

  private CaseRepository repository;

  public CaseDataServiceImpl(@Autowired CaseRepository repository) {
        this.repository = repository;
    }

  public List<Case> listAll() {
        return repository.findAll();
    }
}
