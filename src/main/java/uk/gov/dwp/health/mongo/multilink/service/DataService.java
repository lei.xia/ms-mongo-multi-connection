package uk.gov.dwp.health.mongo.multilink.service;

import java.util.List;

@FunctionalInterface
public interface DataService<T> {

    List<T> listAll();
}
