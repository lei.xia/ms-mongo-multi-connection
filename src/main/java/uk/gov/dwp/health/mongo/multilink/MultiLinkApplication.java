package uk.gov.dwp.health.mongo.multilink;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MultiLinkApplication {
    public static void main(String[] args) {
        SpringApplication.run(MultiLinkApplication.class, args);
    }
}
