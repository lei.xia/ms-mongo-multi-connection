package uk.gov.dwp.health.mongo.multilink.db.fhaappointment;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "appointment")
public class Appointment {
    @Id
    private String id;
    private String supplier;
    private String centre_name;
    private Integer ref_id;
}
